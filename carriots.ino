//https://arduinodiy.wordpress.com/2017/01/12/sending-data-to-carriots-iot-platform-with-esp8266
#include "ESP8266WiFi.h"
const char* ssid="Netgear520"; //replace with your ssid
const char* password = "secret";//replace with your password
const char* server = "api.carriots.com"; 
// Replace with your Carriots apikey
const String APIKEY = "43fxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx8cbb" ;
const String DEVICE = "WeMos-Garden@user.user"; // your deviceID
WiFiClient  client;
int val = 0;
void setup()
{
  Serial.begin(115200);
  delay(1000);// start wifi
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(100);
    Serial.print(".");
    }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
} // Send data to  Carriot
void sendStream()
{
  //const int httpPort = 80;
  if (client.connect(server, 80))
  { // If there's a successful connection
    Serial.println(F("connected"));
    // construct the data json
    String json = "{\"protocol\":\"v2\",\"device\":\"" + DEVICE + "\",\"at\":\"now\",\"data\":{\"moisture\":\"" + val + "\"}}";
    //String json = '{"protocol":"v2","device":"' + DEVICE + '","at":"now","data":{"moisture":"' + val + '"}}';
    // Make an HTTP request
    client.println("POST /streams HTTP/1.1");
    client.println("Host: api.carriots.com");
    client.println("Accept: application/json");
    client.println("User-Agent: Arduino-Carriots");
    client.println("Content-Type: application/json");
    client.print("carriots.apikey: ");
    client.println(APIKEY);
    client.print("Content-Length: ");
    int thisLength = json.length();
    client.println(thisLength);
    client.println("Connection: close");
    client.println();
    client.println(json);
    }else{
    // If server connection failed:
    Serial.println(F("connection failed"));
  }
}
void loop()
{
  val = analogRead(A0);
  Serial.println(val);
  Serial.println(F("Send Data"));
  sendStream();
  delay(30000);
}
